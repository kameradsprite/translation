# Sportvorschrift für das Heer
## A translation of the Wehrmacht Fitness Manual

### File Structure

---

* scan.pdf
  * The original scan of Sportvorschrift für das Heer
* transcription.pdf
  * A typed version of the original scan
* translation.txt
  * A translation of the transcription from german to english
* translation.md
  * A conversion of translation.txt into the markdown format
  
### Sections that need editing:
  
---
  
Note: This lags behind actual progress, look at the issues page to see if someone is already working on a section before you begin to edit. See the editing guide below.
  
- [x] 0-9
- [x] 10-14
- [x] 15-21
- [x] 22-25
- [x] 26-28
- [x] 29-35 
- [x] 36-38
- [x] 39-44
- [x] 45-48
- [x] 49-52 
- [x] 53-58
- [x] 59-66
- [x] 67-69
- [x] 70-72
- [x] 73-83
- [x] 84
- [x] 85-90
- [x] 91-95
- [x] 96-97
- [x] 98-105
- [x] 106-110
- [x] 111-115
- [x] 115-125

### Editing Guide

---

* There are issues corresponding to each section that needs to be edited.  If you want to take a section, add a comment to the issue corresponding to the section you want.  This way there aren't two people working on the same section.  Your edits will be reviewed and then merged into the master branch.
* Only make an edit if a sentence can be made to sound better without changing its meaning.
* If a sentence sounds very off and you question its translation, open an issue about it.
* See "How to Contribute" below

### How to Contribute

---

To edit on GitLab:

1. Click on translation.txt
2. Click edit
3. Make your changes
3. When finished, send your changes as a merge request

To edit locally:

1. Make an account on GitLab
2. Fork this repository by clicking the fork button at the top of the page
3. Make sure git is installed to your computer, [https://git-scm.com](https://git-scm.com)
4. Open the command line and navigate to the folder you want to work from (`cd <working directory>`)
5. Clone your repository to your computer (`git clone <link to your fork>`)
6. Add this repo to your local repo so you can fetch new updates (`git remote add upstream https://gitlab.com/sportvorschrift-fur-das-heer/translation` then `git fetch upstream`)
7. Create a new branch (`git checkout -b <name of your branch, i.e. what you're changing>`)
8. Make your changes
9. Stage your changes (`git add .`)
10. Commit your changes (`git commit -m "<what you changed>"`)
12. Update from the upstream (`git pull upstream master`)
11. Push your changes to GitLab (`git push origin`)
12. Return to your GitLab repo and open a merge request with your branch to have your changes reviewed and added to the main repo

Do not include **<** and **>** in your commands.

### The Plan

---

#### Phase 1: Collection of Material - Complete

* Rip PNGs of Images
* Transcribe the text into an unaltered German version

#### Phase 2: Elaboration of Material

* Clean up/redraw PNGs
* Translate the German Text
* Design a cover for the book

#### Phase 3: Profit

* Put everything together into the best book /pol/ has ever produced
